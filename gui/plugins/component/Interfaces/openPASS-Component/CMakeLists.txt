# /*********************************************************************
# * Copyright (c) 2019 Volkswagen Group of America.
# *
# * This program and the accompanying materials are made
# * available under the terms of the Eclipse Public License 2.0
# * which is available at https://www.eclipse.org/legal/epl-2.0/
# *
# * SPDX-License-Identifier: EPL-2.0
# **********************************************************************/

set(HEADERS
     ${HEADERS}
     ${CMAKE_CURRENT_LIST_DIR}/ComponentInputItemInterface.h
     ${CMAKE_CURRENT_LIST_DIR}/ComponentInputMapInterface.h
     ${CMAKE_CURRENT_LIST_DIR}/ComponentInterface.h
     ${CMAKE_CURRENT_LIST_DIR}/ComponentItemInterface.h
     ${CMAKE_CURRENT_LIST_DIR}/ComponentMapInterface.h
     ${CMAKE_CURRENT_LIST_DIR}/ComponentOutputItemInterface.h
     ${CMAKE_CURRENT_LIST_DIR}/ComponentOutputMapInterface.h
     ${CMAKE_CURRENT_LIST_DIR}/ComponentParameterItemInterface.h
     ${CMAKE_CURRENT_LIST_DIR}/ComponentParameterMapInterface.h
     ${CMAKE_CURRENT_LIST_DIR}/ComponentScheduleInterface.h
     ${CMAKE_CURRENT_LIST_DIR}/ComponentTypedefItemInterface.h
     ${CMAKE_CURRENT_LIST_DIR}/ComponentTypedefMapInterface.h)
