add_subdirectory(common)

add_subdirectory(components/AlgorithmAEB)
add_subdirectory(components/Algorithm_AFDM)
add_subdirectory(components/Algorithm_FmuWrapper)
add_subdirectory(components/Algorithm_Lateral)
add_subdirectory(components/Algorithm_Longitudinal)
add_subdirectory(components/ComponentController)
add_subdirectory(components/Dynamics_Collision)
add_subdirectory(components/Dynamics_TF)
add_subdirectory(components/LimiterAccVehComp)
add_subdirectory(components/OpenScenarioActions)
add_subdirectory(components/SensorAggregation_OSI)
add_subdirectory(components/SensorFusionErrorless_OSI)
add_subdirectory(components/Sensor_Driver)
add_subdirectory(components/Sensor_OSI)
add_subdirectory(components/SignalPrioritizer)

add_subdirectory(core/slave)
add_subdirectory(core/slave/modules/BasicDataStore)
add_subdirectory(core/slave/modules/EventDetector)
add_subdirectory(core/slave/modules/Observation_Log)
add_subdirectory(core/slave/modules/SpawnPointRuntimeCommon)
add_subdirectory(core/slave/modules/SpawnPointScenario)
add_subdirectory(core/slave/modules/SpawnPointWorldAnalyzer)
add_subdirectory(core/slave/modules/World_OSI)
